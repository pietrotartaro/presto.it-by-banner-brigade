// selezioniamo tutti i bottoni categoria
const categoryButtons = document.querySelectorAll('.button-82-pushable');

// per ogni bottone categoria, aggiungiamo gli event listener
categoryButtons.forEach(button => {
  button.addEventListener('mouseover', (event) => {
    // quando avviene l'evento mouseover, mostriamo il nome della categoria
    const categoryName = event.currentTarget.querySelector('#catName');
    categoryName.style.display = 'block';
  });
  
  button.addEventListener('mouseout', (event) => {
    // quando avviene l'evento mouseout, nascondiamo il nome della categoria
    const categoryName = event.currentTarget.querySelector('#catName');
    categoryName.style.display = 'none';
  });
});

function hideCategories(){
    $('.button-82-pushable').addClass('disable-hover'); // aggiungi una classe temporanea per disabilitare l'effetto hover
    $('.category').hide();
    $('.category-choice').removeClass('row');
    $('.category-choice').addClass('column');
    $('.fa-chevron-up').hide();
    $('.fa-chevron-down').show();
    $('.button-82-pushable').removeClass('disable-hover'); // rimuovi la classe temporanea per riattivare l'effetto hover
  }
  