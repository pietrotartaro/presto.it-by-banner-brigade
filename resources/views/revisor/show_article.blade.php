<x-layout>
    <x-slot name="title">{{ $article->title }} - Presto.it</x-slot>
    <div class="container container-custom">
        <div class="row">
            <div class="col-12">
                <h1>{{ $article->title }}</h1>
            </div>
        </div>
        @if ($article)
        <div class="row mt-4">
            <div class="col-12 col-md-6">
                <div id="showCarousel" class="carousel slide" data-bs-ride="carousel">
                    @if ($article->images->isNotEmpty())
                    <div class="carousel-inner img-show">
                        @foreach ($article->images as $image)
                        <div class="carousel-item @if($loop->first)active @endif" >
                            <img src="{{ $image->getUrl(500, 300) }}" class="img-fluid w-100" alt="...">
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="carousel-inner img-show">
                        <div class="carousel-item active" >
                            <img src="https://picsum.photos/500/300" class="img-fluid w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/500/300" class="img-fluid w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/500/300" class="img-fluid w-100" alt="...">
                        </div>
                    </div>
                    @endif
                    <button class="carousel-control-prev" type="button" data-bs-target="#showCarousel" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#showCarousel" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <h5 class="mb-3"><i class="fa-solid fa-user"></i> {{ $article->user->name }}</h5>
                <h4 class="mb-3"><button class="btn-price" disabled>€{{ $article->price }}</button></h4>
                <h5>{{ __('ui.Description')}}<br></h5>
                <p>{{ $article->description }}</p>
                <a class="btn btn-card" href="{{ route('categories.show', $article->category_id) }}"><h5>{{ $article->category->name }}</h5></a>
                <div class="d-flex mt-4 justify-content-evenly">
                    <form action="{{route('revisor.accept_article', [$article])}}" method="POST">
                            @csrf
                            @method('PATCH')
                                <button type="submit" class="btn btn-success">{{ __('ui.Accept')}}</button>
                            </form>
                            <form action="{{route('revisor.reject_article', [$article])}}" method="POST">
                            @csrf
                            @method('PATCH')
                                <button type="submit" class="btn btn-danger mx-2">{{ __('ui.Reject')}}</button>
                    </form>
                    <a href="{{route('revisor.index')}}" class="btn btn-card mx-2">{{ __('ui.BackToDash')}}</a> 
                </div>
            </div>
        </div>
        <!-- api google vision cloud -->
        <div class="container mt-5">
            <div class="row">
                <h5 class="tc-accent">Revisione immagini</h5>
                @foreach($article->images as $image)
                <div class="col-12 col-md-2 my-2">
                    <div class="card-body">
                        <h6 class="tc-accent">Immagine #{{$image->id}}</h6>
                        <p>Adulti: <span class="{{ $image->adult }}"></span></p>
                        <p>Satira: <span class="{{ $image->spoof }}"></span></p>
                        <p>Medicina: <span class="{{ $image->medical }}"></span></p>
                        <p>Violenza: <span class="{{ $image->violence }}"></span></p>
                        <p>Contenuto Ammiccante: <span class="{{ $image->racy }}"></span></p>
                    </div>
                </div>
                @endforeach
                <div class="col-12">
                    <h5 class="tc-accent">Tags</h5>
                    <div class="p-2">
                        @foreach($article->images as $image)
                            @if ($image->labels)
                                @foreach ($image->labels as $label)
                                    <button class="d-inline btn btn-card my-2">{{ $label }} </button>
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- api google test tabella -->

    @endif
    </div>
    </x-layout>