<x-layout>
    <x-slot name="title">Dashboard - Presto.it</x-slot>
<div class="container container-custom">
    <div class="row">
        <h1>Dashboard</h1>
            @if($articles_to_check->isNotEmpty())
                <h3>{{ __('ui.ArticlesToReview')}}</h3>
            @else
                <h3>{{ __('ui.NoArticlesToReview')}}</h3>
            @endif
    </div>
    <div>
        @if(session()->has('message'))
                <div class="alert alert-orange alert-dismissible fade show">
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
        @endif
    </div>
    @if($articles_to_check->isNotEmpty())
    <div class="row">
        <div class="col-12">
            <table class="table mt-5">
            <thead>
                <tr>
                <th scope="col" id="col1">#</th>
                <th scope="col" id="colTitle">{{ __('ui.Title')}}</th>
                <th scope="col" id="colPrice">{{ __('ui.Price')}}</th>
                <th scope="col" id="colCategory">{{ __('ui.Category')}}</th>
                <th scope="col" id="colActions">{{ __('ui.Actions')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($articles_to_check as $article)
                    <tr>
                    <th scope="row" id="col1">{{$article->created_at->diffForHumans()}}</th>
                    <td id="colTitle">{{$article->title}}</td>
                    <td id="colPrice">€ {{$article->price}}</td>
                    <td id="colCategory">{{$article->category->name}}</td>
                    <td class="d-flex" id="colActions">
                        <a href="{{route('revisor.show_article', $article)}}" class="btn btn-card mx-2"><i class="fa-solid fa-eye"></i></a> 
                    </td>
                    </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
    @endif
</div>

</x-layout>