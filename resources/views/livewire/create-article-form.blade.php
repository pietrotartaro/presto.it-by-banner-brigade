<div class="row">
    <div class="col-12 col-md-6">
        <form wire:submit.prevent="store">
        @csrf
        <div class="form-group mb-3">
            <label>{{ __('ui.Title')}}</label>
            <input wire:model="title" type="text" class="form-control @error('title') is-invalid @enderror">
            @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-3">
            <label>{{ __('ui.Category')}}</label>
            <select wire:model="category_id" id="" class="form-control-sm form-control @error('category') is-invalid @enderror">
                <option selected disabled value="null">{{ __('ui.ChooseCategory')}}</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            @error('category')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-3">
            <label>{{ __('ui.Price')}}</label>
            <input wire:model="price" type="number" class="form-control @error('price') is-invalid @enderror">
            @error('price')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-3">
            <label>{{ __('ui.Images')}}</label>
            <input wire:model="temporary_images" type="file" name="images" multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror" placeholder="Img">
            @error('temporary_images.*')
                <div class="text-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group mb-3">
            <label>{{ __('ui.Description')}}</label>
            <textarea wire:model="description" type="text" id="" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror"></textarea>
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div>
            @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
            @endif
        </div>
        <button type="submit" class="btn btn-card">{{ __('ui.UploadArticle')}}</button>
        
        </form>
    </div>
    @if (!empty($images))
            <div class="col-12 col-md-6">
                <p class="m-0">Photo Preview:</p>
                <div class="row border border-4 border-info rounded shadow ">
                    @foreach ($images as $key => $image)
                        <div class="col my-3">
                            <div class="img-preview mx-auto shadow rounded" style="background-image: url({{ $image->temporaryUrl() }});"></div>
                            <button type="button" class="btn btn-danger shadow d-block text-center mt-2 mx-auto" wire:click="removeImage({{ $key }})">{{ __('ui.Delete')}}</button>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
</div>

