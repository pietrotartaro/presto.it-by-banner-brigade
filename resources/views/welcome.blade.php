<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('img/presto-favicon.ico') }}" type="image/x-icon">
    <title>Presto.it</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles
</head>
<body>

    <x-nav/>
    <div>
        @if(session()->has('access.denied'))
                <div class="alert alert-orange alert-dismissible fade show position-absolute alert-home">
                    {{ session('access.denied') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
        @endif
    </div>
    <div>
        @if(session()->has('message'))
                <div class="alert alert-orange alert-dismissible fade show position-absolute alert-home">
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
        @endif
    </div>
    <x-masthead-welcome/>
    <div class="container-fluid" id="dropdownCategory">
        <div class="row">
            <div class="col-12">
            <a class="dropdown ms-2 text-center ">
                <a class="dropdown-toggle text-decoration-none text-light btn" id="categoryBtnDrop" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                {{ __('ui.Categories')}}
                </a>
                <ul class="dropdown-menu">
                @foreach($categories as $category)
                    <li><a class="dropdown-item" href="{{ route('categories.show', $category) }}">{{$category->name}}</a></li>
                @endforeach
                </ul>
            </a>
            </div>
        </div>
    </div>
    <!-- container per bottoni categoria -->
    <div class="container-fluid category-choice"  id="categoryBar">
        <div class="container">
            <div class="row display-2 justify-content-around text-center">
                <div class="col-12" id="categoryCol">
                    @foreach($categories as $category)  
                        <x-masthead-category :category="$category"/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container my-5 ">
        <div class="row justify-content-center">
            <div class="col-12">
                <h1>{{ __('ui.LatestArticlesAdded')}}</h1>
            </div>
        </div>
    </div>
    <div class="container my-5 ">
            <div class="row" id="cardRow">
                @foreach($articles as $article)
                <div class="col-10 col-md-3">
                    <x-articles-card :article="$article"/>
                </div>
               @endforeach
            </div>
    </div>
    <x-footer/>

    @livewireScripts  
    <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</body>
</html>