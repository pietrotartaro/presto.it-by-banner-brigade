<x-layout>
    <x-slot name="title">{{ $article->title }} - Presto.it</x-slot>

    @if($article->is_accepted == true)
    <div class="container container-custom">
        <div class="row">
            <div class="col-12">
                <h1>{{ $article->title }}</h1>
            </div>
        </div>
        <div class="row my-4 justify-content-between">
            <div class="col-12 col-md-6 my-3">
                <div id="showCarousel" class="carousel slide" data-bs-ride="carousel">
                    @if ($article->images->isNotEmpty())
                    <div class="carousel-inner img-show">
                        @foreach ($article->images as $image)
                        <div class="carousel-item @if($loop->first)active @endif" >
                            <img src="{{ $image->getUrl(500, 300) }}" class="img-fluid w-100" alt="...">
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="carousel-inner img-show">
                        <div class="carousel-item active" >
                            <img src="https://picsum.photos/500/300" class="img-fluid w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/500/300" class="img-fluid w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://picsum.photos/500/300" class="img-fluid w-100" alt="...">
                        </div>
                    </div>
                    @endif
                    <button class="carousel-control-prev" type="button" data-bs-target="#showCarousel" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#showCarousel" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <h5 class="mb-3"><i class="fa-solid fa-user"></i> {{ $article->user->name }}</h5>
                <h4 class="my-3"><button class="btn-price" disabled>€{{ $article->price }}</button></h4>
                <hr>
                <p class="my-4">{{ $article->description }}</p>
                <hr>
                <a class="btn btn-card mt-2" href="{{ route('categories.show', $article->category_id) }}"><h5>{{ $article->category->name }}</h5></a>
                @auth
                    @if (Auth::user()->is_revisor)
                        <form action="{{route('revisor.review_article', [$article])}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-card mt-4">{{ __('ui.SendToReview')}}</button>
                        </form>
                    @endif
                @endauth
            </div>
        </div>
    </div>
    <!-- @auth
        @if (Auth::user()->is_revisor)
        <div class="container">
            <div class="row my-5">
                <div class="col-12 col-md-6">
                    <div class="mb-3">
                        <h5>Descrivi il motivo della revisione</h5>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <ul id="reviewMsg">

                    </ul>
                </div>
            </div>
        </div>
        @endif
    @endauth -->
    @else
        <meta http-equiv="refresh" content="0; url=/page-not-found">
    @endif
</x-layout>