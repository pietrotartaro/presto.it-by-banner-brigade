<x-layout>
    <x-slot name="title">{{ __('ui.UploadArticle')}} - Presto.it</x-slot>
    <div class="container container-custom">
        <div class="row">
            <div class="col-12">
                <h1>{{ __('ui.UploadArticle')}}</h1>
            </div>
        </div>
        <livewire:create-article-form />
    </div>








</x-layout>