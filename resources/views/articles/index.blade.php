<x-layout>
    <x-slot name="title">{{ __('ui.AllArticles')}} - Presto.it</x-slot> 
    <div class="container container-custom">
        <div class="row">
            <div class="col-12">
                <h1>{{ __('ui.AllArticles')}}</h1>
            </div>
        </div>
    </div>
        <div class="container my-2">
            <div class="row" id="cardRow">
                @forelse($articles as $article)
                    <div class="col-10 col-md-3 my-3">
                        <x-articles-card :article="$article"/>
                    </div>
                @empty
                    <div class="col-12">
                        <div class="alert alert-blue py-3 shadow">
                            <p>Non ci sono annunci per questa ricerca. Prova a cambiare parola</p>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12 text-center">
                            <h4 class="display-5">{{ __('ui.NewSearch')}}</h6>
                        </div>
                    </div>
                    <div class="row justify-content-center mt-2 mb-5">
                        <div class="col-8">
                            <form class="d-flex" role="search" action="{{route('articles.search')}}" method="GET">
                                <input class="form-control me-2" name="searched" type="search" placeholder="{{ __('ui.Search')}}" aria-label="Search">
                                <button class="btn btn-card" id="btn-nav" type="submit">{{ __('ui.Search')}}</button>
                            </form>
                        </div>
                    </div>
                @endforelse
                
                {{ $articles->links()}}
            </div>
        </div>  
</x-layout>