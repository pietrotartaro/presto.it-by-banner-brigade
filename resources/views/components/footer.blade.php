  <div class="container-fluid pt-5" id="footer">
    <footer class="container">
      <div class="row justify-content-between">
        <div class="col-12 col-md-3 mb-3">
          <h5 id="footerCol">Link</h5>
          <ul class="nav flex-column" id="footerCol">
            <li id="footerItem" class="nav-item mb-2"><a href="{{route('home')}}" class="nav-link p-0 ">Home</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="{{route('articles.index')}}" class="nav-link p-0">{{ __('ui.AllArticles')}}</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="{{route('user.profile')}}" class="nav-link p-0">{{ __('ui.YourProfile')}}</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="{{route('register')}}" class="nav-link p-0">{{ __('ui.Register')}}</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="{{route('login')}}" class="nav-link p-0">{{ __('ui.Login')}}</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="{{route('articles.create')}}" class="nav-link p-0">{{ __('ui.UploadArticle')}}</a></li>
          </ul>
          <!-- MOBILE -->
          <h5 class="dropdown">
            <a class="text-decoration-none nav-link dropdown-toggle" id="mobileChisiamo" role="button" data-bs-toggle="dropdown" aria-expanded="false">Link</a>
            <ul class="dropdown-menu px-2">
              <li class="nav-item mb-2"><a href="{{route('home')}}" class="nav-link p-0  dropdown-item ">Home</a></li>
              <li class="nav-item mb-2"><a href="{{route('articles.index')}}" class="nav-link p-0  dropdown-item">{{ __('ui.AllArticles')}}</a></li>
              <li class="nav-item mb-2"><a href="{{route('user.profile')}}" class="nav-link p-0  dropdown-item">{{ __('ui.YourProfile')}}</a></li>
              <li class="nav-item mb-2"><a href="{{route('register')}}" class="nav-link p-0  dropdown-item">{{ __('ui.Register')}}</a></li>
              <li class="nav-item mb-2"><a href="{{route('login')}}" class="nav-link p-0  dropdown-item">{{ __('ui.Login')}}</a></li>
              <li class="nav-item mb-2"><a href="{{route('articles.create')}}" class="nav-link p-0  dropdown-item">{{ __('ui.UploadArticle')}}</a></li>
            </ul>
          </h5>
          <!-- END -->
        </div>

        <div class="col-12 col-md-3 mb-3">
          <h5 class="dropdown">
          <a class="text-decoration-none nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{ __('ui.Categories')}}</a>
          <ul class="dropdown-menu">
            @foreach($categories as $category)
            <li><a class="dropdown-item" href="{{ route('categories.show', $category) }}">{{$category->name}}</a></li>
            @endforeach
          </ul>
        </h5>
          
        </div> 

        <div class="col-12 col-md-3 mb-3">
          <h5 id="footerCol">{{ __('ui.AboutUs')}}</h5>
          <!-- DESKTOP -->
          <ul class="nav flex-column" id="footerCol">
            <li id="footerItem" class="nav-item mb-2"><a href="#" class="nav-link p-0 disabled">Presto Srl</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="#" class="nav-link p-0 disabled">+39 000 0000 000</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="mailto:info@presto.it" class="nav-link p-0">info@presto.it</a></li>
            <li id="footerItem" class="nav-item mb-2"><a href="#offcanvasFAQs" role="button" data-bs-toggle="offcanvas"  class="nav-link p-0">FAQs</a></li>
          </ul>
          <!-- MOBILE -->
          <h5 class="dropdown">
            <a class="text-decoration-none nav-link dropdown-toggle" id="mobileChisiamo" role="button" data-bs-toggle="dropdown" aria-expanded="false">Chi siamo</a>
            <ul class="dropdown-menu px-2">
              <li class="nav-item mb-2"><a href="#" class="nav-link p-0 disabled dropdown-item">Presto Srl</a></li>
              <li class="nav-item mb-2"><a href="#" class="nav-link p-0 disabled  dropdown-item">+39 000 0000 000</a></li>
              <li class="nav-item mb-2"><a href="mailto:info@presto.it" class="nav-link p-0 dropdown-item">info@presto.it</a></li>
              <li class="nav-item mb-2"><a href="#offcanvasFAQs" role="button" data-bs-toggle="offcanvas"  class=" dropdown-item nav-link p-0">FAQs</a></li>
            </ul>
          </h5>
          <!-- END -->
          <div class='buttons-container'>
            <div class='button facebook text-center'>
              <a href="https://it-it.facebook.com/"><i class="fab fa-facebook-f fa-2x mt-3"></i></a>
            </div>
            <div class='button twitter text-center'>
              <a href="https://twitter.com/"><i class="fab fa-twitter fa-2x mt-3"></i></a>
            </div>
            <div class='button text-center instagram'>
              <a href="https://www.instagram.com/"><i class="fab fa-instagram fa-2x mt-3"></i></a>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-3 mb-3">
          @guest
          <h5>Presto.it</h5>
          <ul class="nav flex-column">
            <li class="nav-item mb-2">{{ __('ui.WorkWithUs')}}</li>
            <li class="nav-item mb-2"><span><a id="footerRegister" href="{{route('register')}}">{{ __('ui.Register')}}</a></span> {{ __('ui.AndClickHere')}}</li>
            <li class="nav-item mb-2"><a href="{{ route('become.revisor') }}" class="btn btn-card text-white">{{ __('ui.BeRevisor')}}</a></li>

          </ul>
          @endguest
          @auth
          @if(!Auth::user()->is_revisor)
          <h5>Presto.it</h5>
          <ul class="nav flex-column">
            <li class="nav-item mb-2">{{ __('ui.WorkWithUs')}}</li>
            <li class="nav-item mb-2"><span><a id="footerRegister" href="{{route('register')}}">{{ __('ui.Register')}}</a></span> {{ __('ui.AndClickHere')}}</li>
            <li class="nav-item mb-2"><a href="{{ route('become.revisor') }}" class="btn btn-card text-white">{{ __('ui.BeRevisor')}}</a></li>

          </ul>
          @endif
          @endauth
          @auth
          @if(Auth::user()->is_revisor)
          <h5>Presto.it</h5>
          <ul class="nav flex-column">
            <li class="nav-item mb-2">{{ __('ui.WorkWithUs')}}</li>
            <li class="nav-item mb-2"><span><a id="footerRegister" href="{{route('register')}}">{{ __('ui.Register')}}</a></span> {{ __('ui.AndClickHere')}}</li>
            <li class="nav-item mb-2"><a href="{{ route('revisor.index')}}" class="btn btn-card text-white">{{ __('ui.BeRevisor')}}</a></li>

          </ul>
          @endif
          @endauth
        </div>
        
      </div>

  
      
    
          
    </footer>
  </div>
    
    <!-- offcanvas -->
      <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasFAQs" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-header">
          <h5 class="offcanvas-title" id="offcanvasExampleLabel">Domande frequenti</h5>
          <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="offcanvas-body">
          <div>
            <p>Qui potrai trovare le risposte a gran parte delle tue domande.</p>
            <p>Per ulteriori informazioni <a class="nav-link text-primary" href="mailto:info@presto.it">contattaci.</a></p>
          </div>
          <div class="accordion mt-5" id="accordionExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Qual è il vostro prodotto/servizio principale?
                </button>
              </h2>
              <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong>Presto.it è un sito di annunci online che permette a privati e aziende di inserire annunci gratuiti di prodotti e servizi.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Quali sono i vostri orari di apertura?
                </button>
              </h2>
              <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> Presto.it è un sito web accessibile 24 ore al giorno, 7 giorni su 7.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Come posso contattarvi?
                </button>
              </h2>
              <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> È possibile contattare il servizio clienti di Presto.it attraverso il form di contatto presente sul sito o via email all'indirizzo <a href="mailto:info@presto.it">info@presto.it</a>.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  Quali sono le modalità di pagamento accettate?
                </button>
              </h2>
              <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> Presto.it non effettua transazioni monetarie. Gli annunci inseriti sul sito sono gratuiti e ogni eventuale transazione economica viene gestita direttamente tra il venditore e l'acquirente.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingFive">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                  Come posso restituire o cambiare un prodotto?
                </button>
              </h2>
              <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> Presto.it non effettua la vendita diretta di prodotti, quindi non è coinvolto nelle eventuali operazioni di reso o sostituzione. Si consiglia di contattare direttamente il venditore in caso di problemi.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingSix">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                  Quali sono le vostre politiche di spedizione e consegna?
                </button>
              </h2>
              <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> Presto.it non effettua la spedizione o la consegna di prodotti, in quanto si tratta di un sito di annunci. Le condizioni di spedizione e consegna sono stabilite direttamente tra il venditore e l'acquirente.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingSeven">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                  Quali sono le vostre politiche sulla privacy?
                </button>
              </h2>
              <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> Presto.it si impegna a tutelare la privacy degli utenti. Per maggiori informazioni sul trattamento dei dati personali, si consiglia di consultare la sezione Privacy Policy presente sul sito.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingEight">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                Come posso accedere al mio account o recuperare la mia password?
                </button>
              </h2>
              <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> È possibile accedere all'account personale su Presto.it attraverso la pagina di login. In caso di smarrimento della password, è possibile effettuare il recupero tramite l'apposita funzione presente sulla pagina di login.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingNine">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                  Come posso inserire un annuncio sul sito Presto.it?
                </button>
              </h2>
              <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong> Per inserire un annuncio su Presto.it, è necessario registrarsi e accedere all'area personale. Una volta effettuato l'accesso, selezionare la categoria di appartenenza dell'annuncio e compilare il modulo di inserimento, inserendo una descrizione dettagliata del prodotto o servizio offerto e le relative immagini. Una volta completati tutti i campi richiesti, l'annuncio sarà pubblicato sul sito.
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingTen">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                Quali sono le modalità per promuovere il mio annuncio su Presto.it?
                </button>
              </h2>
              <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <strong>La risposta alla tua domanda.</strong>Presto.it offre diverse opzioni per promuovere gli annunci, tra cui l'inserimento di immagini aggiuntive, l'evidenziazione dell'annuncio in homepage e la pubblicazione in evidenza nella categoria di appartenenza. Inoltre, è possibile utilizzare i servizi di advertising offerti dal sito per aumentare la visibilità dell'annuncio. Tutte queste opzioni possono essere selezionate durante il processo di inserimento dell'annuncio o successivamente, dall'area personale.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
