<button class="button-82-pushable" role="button">
    <span class="button-82-shadow"></span>
    <span class="button-82-edge"></span>
    <a href="{{ route('categories.show', $category) }}" class="text-decoration-none">
        <span class="button-82-front">
            {!!$category->icon->body!!}
            <h6 class="mt-2  category-name" id="catName">
                | {{$category->name}}
            </h6>
        </span>
    </a>
</button>

