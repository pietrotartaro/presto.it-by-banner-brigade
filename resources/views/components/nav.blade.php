<nav class="navbar navbar-expand-lg fixed-top mynavbar" id="mynavbar">
    <div class="container">
        <a class="navbar-brand navBrand fontBold ms-3" id="navBrand" href="{{route('home')}}"> <img
                src="/img/PRESTO_LOGO.jpg" style="height:50px" class="me-3" alt=""></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="text-white fa-solid fa-bars"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                @auth
                <li class="nav-item">
                    <a class="nav-link navlink ms-2" aria-current="page" href="{{route('articles.create')}}">{{ __('ui.UploadArticle')}}</a>
                </li>
                @endauth
                <li class="nav-item ">
                    <a class="nav-link navlink ms-2" href="{{route('articles.index')}}">{{ __('ui.AllArticles')}}</a>
                </li>
                <li class="nav-item dropdown ms-2">
                    <a class="nav-link navlink dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">{{ __('ui.Categories')}}</a>
                    <ul class="dropdown-menu">
                        @foreach($categories as $category)
                        <li><a class="dropdown-item"
                                href="{{ route('categories.show', $category) }}">{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
            <ul class="navbar-nav align-items-end me-3 mb-2 mb-lg-0">
                @if (Auth::user() == null)
                <li class="nav-item">
                    <a class="nav-link navlink" href="{{ route('register') }}">{{ __('ui.Register')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link navlink" href="{{ route('login') }}">{{ __('ui.Login')}}</a>
                </li>
                @endif
            </ul>
            <!-- PROFILO UTENTE -->
            @auth
            <li class="nav-item dropdown">
                    <a class="nav-link navlink dropdown-toggle align-items-center" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="fa-solid fa-user"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-start px-2 mt-2" id="btnDropdownLeft">
                        <li class="dropdown-item">
                            <a class="nav-link navlink" href="{{ route('user.profile') }}">{{ Auth::user()->name }}</a>
                        </li>
                        <li class="dropdown-item">
                            @if(Auth::user()->is_revisor)
                                <a class="nav-link navlink  position-relative" href="{{route('revisor.index')}}">{{ __('ui.RevisorDashboard')}}
                                    @if(App\Models\Article::toBeRevisionedCount() > 0)
                                        <span
                                        class="position-absolute ms-2 top-0 start-md-100 translate-middle badge rounded-pill bg-danger">
                                        {{App\Models\Article::toBeRevisionedCount()}}
                                        <span class="visually-hidden">messaggi non letti</span>
                                        </span>
                                    @endif
                                </a>
                            @endif
                        </li>
                        <li class="dropdown-item">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button class="btn btn-dropdown" id="btnOut">Logout</button>
                            </form>
                        </li>
                    </ul>
            </li>
            @endauth

            <!-- switch lingue -->
            <li class="nav-item dropdown mx-3">
                <a class="nav-link navlink dropdown-toggle align-items-center" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Lang
                </a>
                    <ul class="dropdown-menu dropdown-menu-start mt-2">
                        <li class="dropdown-item">
                            <x-_locale lang="it" />
                        </li>
                        <li class="dropdown-item">
                            <x-_locale lang="en" />
                        </li>
                        <li class="dropdown-item">
                            <x-_locale lang="es" />
                        </li>
                    </ul>
            </li>
            <form class="d-flex" role="search" action="{{route('articles.search')}}" method="GET">
                <input class="form-control me-2" name="searched" type="search" placeholder="{{ __('ui.Search')}}" aria-label="Search">
                <button class="btn btn-nav" id="btn-nav" type="submit">{{ __('ui.Search')}}</button>
            </form>
        </div>
    </div>
</nav>