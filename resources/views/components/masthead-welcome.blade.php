
    <header class="masthead">
        <div class="container h-100 ">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-12 col-md-4">
                        <img src="./img/PRESTO.png" alt="" class="masthead-icon">
                    </div>
                    <div class="col-12 col-md-4">
                        <img src="./img/undraw_web_shopping_re_owap.svg" alt="" class="masthead-image">
                        <div class="d-flex justify-content-between position-relative">
                            <a class="text-decoration-none" href="{{route('articles.index')}}"><button class="button-21 fw-bold" role="button">{{ __('ui.Buy')}}</button></a>
                            <a class="text-decoration-none" href="{{route('articles.create')}}"><button class="button-21 fw-bold" role="button">{{ __('ui.Sell')}}</button></a>
                        </div >
                    </div>
                    <div class="col-12 col-md-4">
                        <h1 class="fw-light text-end masthead-h1">{{ __('ui.BuyAndSell')}}</h1>
                        <p class="masthead-p text-end"><span class="auto-type typed"></span></p>
                    </div>

                </div>
            </div>
        </div>
    </header>
