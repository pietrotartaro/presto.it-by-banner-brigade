
    <div class="card rounded-4 my-2">
      <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(500, 500) : 'https://picsum.photos/200'}}" class="card-img-top p-2 rounded-4" alt="">
      <div class="card-body">
        <a href="{{ route('categories.show', $article->category) }}" class="my-2 btn btn-card">{{ $article->category->name }}</a>
        <h5 class="card-title mt-2">
          {!! substr($article->title, 0, 15)!!} 
          @if(strlen($article->title) >= 15)... @endif
        </h5>
        <p class="card-text">€ {{$article->price}}</p>
        @if($article->is_accepted === null)
        <button class="my-2 btn btn-card" disabled>{{ __('ui.ArticleUnderReview')}}</button>
        @elseif($article->is_accepted == true)
        <a href="{{ route('articles.show', $article) }}"><x-read-more-btn/></a> 
        @elseif($article->is_accepted == false)
        <button class="my-2 btn btn-card" disabled>{{ __('ui.ArticleRejected')}}</button>
        @endif
        <div class="line">
          <div class="card-footer mt-3 ">
            <p class="m-0"><span>{{ $article->created_at->format('d-m-Y')}}</span></p>
          </div>
        </div>
      </div>
    </div>

