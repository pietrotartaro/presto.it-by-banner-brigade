<form class="d-inline" id="formLocale" action="{{ route('setLocale', $lang)}}" method="POST">
    @csrf
    <button type="submit" class="btn btn-dropdown" id="btnLang">
        <span>{{ strtoupper($lang) }}</span>
    </button>
</form>