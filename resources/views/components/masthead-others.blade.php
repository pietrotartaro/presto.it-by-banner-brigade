
    <header class="masthead-others container-fluid">
            <div class="row">
                <div class="col-12 col-md-4 d-flex align-items-center justify-content-center">
                    <img src="/img/compra.svg" alt="compra" class="img-others">
                </div>
                <div class="col-12 col-md-4 mobile-button">
                    <a class="text-decoration-none me-md-5 mt-md-5" href="{{route('articles.index')}}"><button class="btn btn-masthead fw-bold p-0" role="button">{{ __('ui.Buy')}}</button></a>
                    <a class="text-decoration-none ms-md-3 mt-3 mt-md-5" href="{{route('articles.create')}}"><button class="btn btn-masthead fw-bold p-0" role="button">{{ __('ui.Sell')}}</button></a>
                </div>
                <div class="col-12 col-md-4 d-flex align-items-center justify-content-center">
                    <img src="/img/vendi.svg" alt="vendi" class="img-others">
                </div>
            </div >
    </header>
