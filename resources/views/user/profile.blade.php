<x-layout>
    <div class="container container-custom">
        <div class="row">
            <h1>{{ __('ui.Welcome')}}, {{ Auth::user()->name }}</h1>
        </div>
        <div class="row">
            <div class="col-12">
                <h2>{{ __('ui.YourArticles')}}</h2>
            </div>
        </div>
        <div class="row my-5">
            @foreach (Auth::user()->articles as $article)
            <div class="col-12 col-md-3">
                <x-articles-card
                :article="$article"
                ></x-articles-card>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>