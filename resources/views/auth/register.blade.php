<x-layout :showMasthead="$showMasthead">
    <x-slot name="title">{{ __('ui.Register')}} - Presto.it</x-slot>

<div class="container" id="registerId">
        <div class="row">
            <div class="col-12">
                <div class="wrapper">
                    <input type="radio" name="nav" id="home" />
                    <input type="radio" name="nav" id="user" checked="checked"/>

                    
                    <aside>
                    <div class="aside__inner">
                    <label for="home"><svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><title>home</title><path d="M4.48 17.93v9.776c0 .576.47 1.047 1.047 1.047h7.156c.576 0 1.048-.47 1.048-1.047V20.55h4.364v7.156c0 .576.47 1.047 1.047 1.047h6.81c.575 0 1.046-.47 1.046-1.047V17.93h2.967c.42 0 .803-.26.96-.644.157-.384.07-.838-.227-1.135L17.046 2.31c-.4-.402-1.064-.42-1.465-.017L1.32 16.133c-.313.297-.4.75-.244 1.153.157.4.54.645.96.645H4.48z"/></svg></label>
                    
                    <label for="user"><svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><title>user</title><g><ellipse cx="16" cy="9.903" rx="6.903" ry="6.903"/><path d="M27.505 28.31c0-6.354-5.15-11.505-11.505-11.505-6.354 0-11.505 5.15-11.505 11.505"/></g></svg></label>

                    
                    <span class="background"></span>
                        </div>
                    </aside>
                    <div class="content "  id="registerContent">

                    
                    <div class="content__view content__user">
                        <h1 id="login">{{ __('ui.Register')}}</h1>
                        <form method="POST" action="{{ route('register') }}"  id="login-form">
                            @csrf
                            <label>{{ __('ui.Name')}}:</label>
                            <input type="text" name="name" placeholder="{{ __('ui.YourName')}}" required >
                                @error('name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            <label>Email:</label>
                            <input type="email" name="email" placeholder="{{ __('ui.YourEmail')}}" required >
                            @error('email')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <label>Password:</label>
                            <input type="password" name="password" placeholder="{{ __('ui.YourPsw')}}" required >
                            @error('password')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <label>Password:</label>
                            <input type="password" name="password_confirmation" placeholder="{{ __('ui.ConfirmPsw')}}" required>
                            @error('password_confirmation')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <button type="submit" class="btn" id="registerBtn">{{ __('ui.Register')}}</button>

                        </form>
                    </div>
                    
                    
                    
                
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-layout>