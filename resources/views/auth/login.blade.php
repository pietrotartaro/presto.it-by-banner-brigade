<x-layout  :showMasthead="$showMasthead">
    <x-slot name="title">{{ __('ui.Login')}} - Presto.it</x-slot>

    <!-- custom login card -->
    <div class="container " id="loginId">
        <div class="row">
            <div class="col-12">
                <div class="wrapper">
                    <input type="radio" name="nav" id="home" />
                    <input type="radio" name="nav" id="user" checked="checked"/>

                    
                    <aside>
                    <div class="aside__inner">
                    <label for="home"><svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><title>home</title><path d="M4.48 17.93v9.776c0 .576.47 1.047 1.047 1.047h7.156c.576 0 1.048-.47 1.048-1.047V20.55h4.364v7.156c0 .576.47 1.047 1.047 1.047h6.81c.575 0 1.046-.47 1.046-1.047V17.93h2.967c.42 0 .803-.26.96-.644.157-.384.07-.838-.227-1.135L17.046 2.31c-.4-.402-1.064-.42-1.465-.017L1.32 16.133c-.313.297-.4.75-.244 1.153.157.4.54.645.96.645H4.48z"/></svg></label>
                    
                    <label for="user"><svg width="32" height="32" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><title>user</title><g><ellipse cx="16" cy="9.903" rx="6.903" ry="6.903"/><path d="M27.505 28.31c0-6.354-5.15-11.505-11.505-11.505-6.354 0-11.505 5.15-11.505 11.505"/></g></svg></label>

                    
                    <span class="background"></span>
                        </div>
                    </aside>
                    <div class="content" id="loginContent">
                    @auth
                    <div class="content__view content__home">
                        <h1 id="login">{{ __('ui.Login')}}</h1>
                        <p>Qui puoi gestire il tuo profilo</p>
                        <a for="user" class="btn" id="login" href="{{route('user.profile')}}">Modifica il tuo profilo</a>
                        <a for="stat" class="btn" id="login">Visualizza i tuoi articoli</a>
                        <a for="lock" class="btn" id="login">Carica un nuovo articolo</a>
                    </div>
                    @endauth
                    @guest
                    <div class="content__view content__home" disabled>
                        <h1 id="login">{{ __('ui.Login')}}</h1>
                        <p>Accedi per gestire il tuo profilo</p>
                        <a for="user" class="btn disabled" id="login" disabled>Modifica il tuo profilo</a>
                        <a for="stat" class="btn disabled" id="login" disabled>Visualizza i tuoi articoli</a>
                        <a for="lock" class="btn disabled" id="login" disabled>Carica un nuovo articolo</a>
                    </div>
                    
                    <div class="content__view content__user">
                        <h1 id="login">{{ __('ui.Login')}}</h1>
                        <form method="POST" action="{{ route('login') }}"  id="login-form">
                            @csrf
                            <label>Email:</label>
                            <input type="email" name="email" placeholder="{{ __('ui.YourEmail')}}" required >
                                @error('email')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            <label>Password:</label>
                            <input type="password" name="password" placeholder="*********" required>
                            @error('password')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <button type="submit" class="btn d-block"  id="registerBtn">{{ __('ui.Login')}}</button>

                        </form>
                    </div>
                    @endguest
                    
                    
                
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end custom login card -->


    
</x-layout>