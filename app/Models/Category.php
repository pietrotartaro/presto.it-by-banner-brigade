<?php

namespace App\Models;

use App\Models\Icon;
use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'icon_id'
    ];

    public function articles(){
        return $this->hasMany(Article::class);
    }

    public function icon()
    {
        return $this->belongsTo(Icon::class);
    }
}
