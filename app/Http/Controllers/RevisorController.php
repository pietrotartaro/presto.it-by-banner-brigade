<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Mail\BecomeRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index(){
        $articles_to_check = Article::where('is_accepted', null)->orderBy('created_at', 'desc')->get();
        return view('revisor.index', compact('articles_to_check'));
    }

    public function reviewArticle(Article $article){
        $article->setAccepted(null);
        return redirect('/revisor/home')->with('message', 'Annuncio tornato in revisione');
    }

    public function acceptArticle(Article $article){
        $article->setAccepted(true);
        return redirect('/revisor/home')->with('message', 'Annuncio approvato correttamente');
    }

    public function rejectArticle(Article $article){
        $article->setAccepted(false);
        return redirect('/revisor/home')->with('message', 'Annuncio rifiutato');
    }

    public function showArticle(Article $article){
        return view('revisor.show_article', compact('article'));
    }

    public function becomeRevisor(){
        Mail::to('admin@presto.com')->send(new BecomeRevisor(Auth::user()));
        return redirect()->back()->with('message', 'Richiesta per diventare revisore inviata correttamente');
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:makeUserRevisor', ["email" => $user->email]);
        return redirect('/')->with('message', 'Complimenti! L\'utente è diventato revisore');
    }
}
