<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function create() {
        return view('articles.create');
    }

    public function index(){
        $articles = Article::where('is_accepted', true)->orderByDesc('created_at')->paginate(8);
        return view('articles.index', compact('articles'));
    }

    public function show(Article $article){
        return view('articles.show', compact('article'));
    }
}
