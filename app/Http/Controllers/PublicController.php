<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home() {
        $articles = Article::where('is_accepted', true)->take(8)->get()->sortByDesc('created_at');
        return view('welcome', compact('articles'));
    }

    public function profile(){
        return view('user.profile');
    }

    public function categoryShow(Category $category){
        
        return view('categories.category-show', compact('category'));
    }

    public function searchArticles(Request $request) {
        $articles = Article::search($request->searched)->where('is_accepted', true)->paginate(8);
        return view('articles.index', compact('articles'));
    }

    public function setLanguage($lang){
        
        session()->put('locale', $lang);
        return redirect()->back();
    }
}
