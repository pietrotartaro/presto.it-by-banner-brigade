<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateArticleForm extends Component
{
    use WithFileUploads;

    public $title;
    public $description;
    public $price;
    public $category_id;
    public $images = [];
    public $temporary_images;
    public $article;

    protected $rules = [
        'title' => 'required|min:5|max:50',
        'description' => 'required|min:20',
        'price' => 'required|numeric',
        'category_id' => 'required',
        'images.*' => 'image|max:1024',
        'temporary_images.*' => 'image|max:1024',

    ];

    protected $messages = [
        'title.required' => 'Il titolo è obbligatorio.',
        'description.required' => 'La descrizione è obbligatorio.',
        'price.required' => 'Il prezzo è obbligatorio.',
        'min' => 'Questo campo è troppo corto.',
        'max' => 'Questo campo è troppo lungo.',
        'temporary_images.required' => 'L\'immagine è richiesta',
        'temporary_images.*.image' => 'L\'immagine deve essere un\'immagine',
        'temporary_images.*.max' => 'L\'immagine deve essere massimo di 1 MB',
        'images.image' => 'L\'immagine deve essere un\'immagine',
        'image.max' => 'L\'immagine deve essere massimo di 1 MB',
    ];

    public function updatedTemporaryImages(){
        if ($this->validate([
            'temporary_images.*' => 'image|max:1024',
        ])) {
            foreach ($this->temporary_images as $image) {
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key){
        if (in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function store() {
        $this->validate();

        $this->article = Category::find($this->category_id)->articles()->create($this->validate());
        if (count($this->images)) {
            foreach ($this->images as $image) {
                $newFileName = "articles/{$this->article->id}";
                $newImage = $this->article->images()->create(['path'=>$image->store($newFileName, 'public')]);


                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 500, 500),
                    new ResizeImage($newImage->path, 500, 300),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id)
                ])->dispatch($newImage->id);
            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }
        
        $this->article->user()->associate(Auth::user());
        $this->article->save();
        
        session()->flash('message', 'Annuncio inserito correttamente, sarà pubblicato dopo la revisione');
        $this->cleanForm();
    }

    public function cleanForm(){
        $this->title = '';
        $this->description = '';
        $this->price = '';
        $this->category_id = '';
        $this->images = [];
        $this->temporary_images = [];
    }

    public function render()
    {
        $categories = Category::all();
        return view('livewire.create-article-form', compact('categories'));
    }
}
