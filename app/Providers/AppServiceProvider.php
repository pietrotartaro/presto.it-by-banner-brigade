<?php

namespace App\Providers;

use App\Models\Icon;
use App\Models\Category;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::HasTable('categories')){
            View::share('categories', Category::all());
        }
        if (Schema::HasTable('icons')){
            View::share('icons', Icon::all());
        }


        Paginator::useBootstrapFive();
        Paginator::useBootstrapFour();

        view()->share('showMasthead', true);
    }
}
