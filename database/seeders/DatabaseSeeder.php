<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Icon;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        // \App\Models\User::factory(10)->create();

         \App\Models\User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@example.com',
             'password' => Hash::make('asdasdasd'),
        ]);
        $icons = [
            '<i class="fa-solid fa-couch"></i>', '<i class="fa-solid fa-shirt"></i>',  '<i class="fa-solid fa-microchip"></i>', '<i class="fa-solid fa-book"></i>', '<i class="fa-solid fa-music"></i>','<i class="fa-solid fa-futbol"></i>', '<i class="fa-solid fa-motorcycle"></i>','<i class="fa-solid fa-hands-holding-child"></i>', '<i class="fa-solid fa-paw"></i>', '<i class="fa-solid fa-record-vinyl"></i>'
        ];
        foreach($icons as $body){
            Icon::create(
                [
                    'body' => $body,
                ]
            );
        }
        $categories = [
            'Arredo' => 1,
            'Abbigliamento' => 2,
            'Tecnologia' => 3,
            'Libri' => 4 ,
            'Musica e Film' => 5,
            'Sport' => 6 ,
            'Motori' => 7,
            'Cura della persona' => 8,
            'Accessori per animali' => 9,
            'Collezionismo' => 10
        ];
        
        foreach($categories as $name => $icon_id){
            Category::create(
                [
                    'name' => $name,
                    'icon_id' => $icon_id
                ]
            );
        }

        // SEEDER ARTICOLI


    }
}
