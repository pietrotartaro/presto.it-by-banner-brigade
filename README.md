        $this->article->user()->associate(Auth::user());
        $this->article->save();


        Article::create([
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
            'category_id' => $this->category_id,
            'user_id' => Auth::User()->id,
        ]);


    public $category_id;


    <div class="container container-custom">
        <div class="row">
            <div class="col-12">
                <h1>
                    Tutti gli annunci
                </h1>
            </div>
        </div>
    </div>

    <!-- vecch ibottoni categoria -->
<!-- <button class="button-82-pushable" role="button">
    <span class="button-82-shadow"></span>
    <span class="button-82-edge"></span>
    <a href="{{ route('categories.show', $category) }}" class="text-decoration-none">
        <span class="button-82-front text">
            <h6 class="mb-0">{{$category->name}}</h6>
        </span>
    </a>
    <a class="text-light" href="{{ route('categories.show', $category) }}">{!!$category->icon->body!!}</a>
</button> -->




tabella api revisore
        <!-- <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="table mt-5">
                        <thead>
                            <tr>
                            <th scope="col" id="col1">Adulti</th>
                            <th scope="col" id="colTitle">Satira</th>
                            <th scope="col" id="colPrice">Medicina</th>
                            <th scope="col" id="colCategory">Violenza</th>
                            <th scope="col" id="colActions">Contenuto Ammiccante</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($article as $image)
                                <tr>
                                <th scope="row" class="{{ $image->adult }}"></th>
                                <td id="colTitle">{{$article->title}}</td>
                                <td id="colPrice">{{$article->price}}</td>
                                <td id="colCategory">{{$article->category->name}}</td>
                                <td class="d-flex" id="colActions">
                                    <a href="{{route('revisor.show_article', $article)}}" class="btn btn-card mx-2"><i class="fa-solid fa-eye"></i></a> 
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> -->




## ARTICOLI SEEDER
        $articles = [
            [
                'title' => 'Divano in pelle',
                'price' => 499.99,
                'description' => 'Divano in vera pelle, colore marrone, 3 posti',
                'category_id' => 1
            ],
            [
                'title' => 'Maglione di lana',
                'price' => 79.99,
                'description' => 'Maglione in lana merino, colore grigio, taglia M',
                'category_id' => 2
            ],
            [
                'title' => 'Smartphone Samsung',
                'price' => 599.99,
                'description' => 'Smartphone Samsung Galaxy S21, 128GB, colore nero',
                'category_id' => 3
            ],
            [
                'title' => 'Il signore degli anelli',
                'price' => 29.99,
                'description' => 'Libro Il signore degli anelli, edizione deluxe illustrata',
                'category_id' => 4
            ],
            [
                'title' => 'CD Greatest Hits',
                'price' => 14.99,
                'description' => 'CD Greatest Hits dei Queen, 20 brani',
                'category_id' => 5
            ],
            [
                'title' => 'Palla da calcio',
                'price' => 19.99,
                'description' => 'Palla da calcio Adidas, taglia 5, colore bianco e nero',
                'category_id' => 6
            ],
            [
                'title' => 'Auto usata',
                'price' => 8999.99,
                'description' => 'Auto usata Fiat 500, anno 2018, 50.000 km',
                'category_id' => 7
            ],
            [
                'title' => 'Set di trucchi',
                'price' => 39.99,
                'description' => 'Set di trucchi professionale, 24 pezzi',
                'category_id' => 8
            ],
            [
                'title' => 'Collare per cane',
                'price' => 9.99,
                'description' => 'Collare in pelle per cane, colore rosso, taglia L',
                'category_id' => 9
            ],
            [
                'title' => 'Moneta antica',
                'price' => 499.99,
                'description' => 'Moneta antica romana, del 50 a.C.',
                'category_id' => 10
            ],
            [
                'title' => 'Borsa di pelle',
                'price' => 149.99,
                'description' => 'Borsa di pelle nera, con tracolla e tasca esterna',
                'category_id' => 2
            ],
            [
                'title' => 'Computer portatile',
                'price' => 1199.99,
                'description' => 'Computer portatile Dell XPS 15, processore Intel Core i7, 16GB di RAM',
                'category_id' => 3
            ]
            ];

        foreach ($articles as $article) {
            Article::create([
                'title' => $article['title'],
                'price' => $article['price'],
                'description' => $article['description'],
                'category_id' => $article['category_id']
            ]);
        }