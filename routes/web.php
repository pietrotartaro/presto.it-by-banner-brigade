<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RevisorController;


Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('article/show/{article}', [ArticleController::class, 'show'])->name('articles.show');
Route::get('category/show/{category}', [PublicController::class, 'categoryShow'])->name('categories.show');
Route::get('make/revisor/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');
Route::get('search/article', [PublicController::class, 'searchArticles'])->name('articles.search');
Route::get('article/index',[ArticleController::class, 'index'])->name('articles.index');
Route::post('language/{lang}',[PublicController::class, 'setLanguage'])->name('setLocale');

Route::middleware(['auth'])->group(function () {
   Route::get('user/profile', [PublicController::class, 'profile'])->name('user.profile');
   Route::get('article/create', [ArticleController::class, 'create'])->name('articles.create');
   Route::get('request/revisor', [RevisorController::class, 'becomeRevisor'])->name('become.revisor');

});

Route::middleware(['isRevisor'])->group(function () {
   Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisor.index');
   Route::patch('/accept/article/{article}', [RevisorController::class, 'acceptArticle'])->name('revisor.accept_article');
   Route::patch('/reject/article/{article}', [RevisorController::class, 'rejectArticle'])->name('revisor.reject_article');
   Route::get('/show/article/{article}', [RevisorController::class, 'showArticle'])->name('revisor.show_article');
   Route::patch('/review/article/{article}', [RevisorController::class, 'reviewArticle'])->name('revisor.review_article');
   

});

